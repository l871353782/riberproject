﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Words;

namespace OperateWordDemo
{
    public partial class InsertWordFromHtml : Form
    {
        public InsertWordFromHtml()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Document doc = new Document();
            DocumentBuilder builder = new DocumentBuilder(doc);
            string html = FromFileReadToString(AppDomain.CurrentDomain.BaseDirectory + "Template\\orderTemplate.html");           
            builder.InsertHtml(html);
            doc.Save(AppDomain.CurrentDomain.BaseDirectory + "DocumentBuilder.InsertTableFromHtml Out.doc");
            // doc.Range.Bookmarks.Clear();//清除书签
            //  doc.Save(DateTime.Now.ToString("yyyy-MM-dd") + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  
        }

        public  string FromFileReadToString(string fileName)
        {
            TextReader txtReader = new StreamReader(fileName, Encoding.Default);
            string s = txtReader.ReadToEnd();
            txtReader.Close();
            return s;
        }

    }
}
