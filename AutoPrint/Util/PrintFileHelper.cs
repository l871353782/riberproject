﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AutoPrint.Util
{
    public class PrintFileHelper
    {
        private const string PrintServerName = "YULIN-PC";
        private const string PrintName = "HP LaserJet M1522 series PCL6 Class Driver";
        /// <summary>
        /// 打印控件
        /// </summary>
        /// <param name="element"></param>
        public void PrintVisual(FrameworkElement element)
        {
            var printDialog = new PrintDialog();
            SetPrintProperty(printDialog);
            var printQueue = SelectedPrintServer();
            if (printQueue != null)
            {
                printDialog.PrintQueue = printQueue;
                printDialog.PrintVisual(element, "");
            }
        }
        /// <summary>
        /// 查找并获取打印机
        /// </summary>
        /// <param name="printerServerName">服务器名字： Lee-pc</param>
        /// <param name="printerName">打印机名字：Hp laserjet m1522 mfp series pcl 6 </param>
        /// <returns></returns>
        public PrintQueue SelectedPrintServer(string printerServerName, string printerName)
        {
            try
            {
                var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
                PrintServer printServer = null;
                foreach (string printer in printers)
                {
                    if (printer.Contains(printerName))
                        printServer = new PrintServer("////" + printerServerName);
                }
                if (printServer == null) return null;//没有找到打印机服务器
                var printQueue = printServer.GetPrintQueue(printerName);
                return printQueue;
            }
            catch (Exception)
            {
                return null;//没有找到打印机
            }
        }

        /// <summary>
        /// 查找并获取打印机
        /// </summary>
        /// <returns></returns>
        public PrintQueue SelectedPrintServer()
        {
            try
            {
                var printers = PrinterSettings.InstalledPrinters;//获取本机上的所有打印机
                PrintServer printServer = null;
                if (printers.Count == 0) return null;//没有找到打印机服务器
                string[] parList = printers[5].Split('\\');
                printServer = new PrintServer(parList[0]);
                var printQueue = printServer.GetPrintQueue(parList[1]);
                return printQueue;
            }
            catch (Exception ex)
            {
                return null;//没有找到打印机
            }
        }

        /// <summary>
        /// 设置打印格式
        /// </summary>
        /// <param name="printDialog">打印文档</param>
        /// <param name="pageSize">打印纸张大小 a4</param>
        /// <param name="pageOrientation">打印方向 竖向</param>
        public void SetPrintProperty(PrintDialog printDialog, PageMediaSizeName pageSize = PageMediaSizeName.ISOA4, PageOrientation pageOrientation = PageOrientation.Portrait)
        {
            var printTicket = printDialog.PrintTicket;
            printTicket.PageMediaSize = new PageMediaSize(pageSize);//A4纸
            printTicket.PageOrientation = pageOrientation;//默认竖向打印
        }

    }
}
