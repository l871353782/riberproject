﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordPrint
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            if (!Directory.Exists(path))//判断是否存在
            {
                Directory.CreateDirectory(path);//创建新路径
            }
            List<string> printer = GetAllPrinter();
            foreach (var item in printer)
            {
                this.cmbPrinter.Items.Add(item);
            }
        }
        /// <summary>
        /// 文档path
        /// </summary>
        string path = Application.StartupPath + "/请放入要打印的文件";
        bool isStart = false;
        bool pause = false;
        Thread t = null;





        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(path);
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            int isCanResult = IsCanPrint();
            if (isCanResult == 0)
            {
                LoadWord();
            }
            else if (isCanResult == 1)
            {
                MessageBox.Show("文件夹中不能有除doc和docx格式的其他文件！");
            }
            else if (isCanResult == 3)
            {
                MessageBox.Show("文件夹为空！请放入doc文件。");
                btnOpenFolder_Click(new object(), new EventArgs());
            }
        }

        /// <summary>
        /// 检查文件格式
        /// </summary>
        /// <returns></returns>
        private int IsCanPrint()
        {
            int result = 0;
            DirectoryInfo mydir = new DirectoryInfo(path);
            FileSystemInfo[] fsis = mydir.GetFileSystemInfos();
            if (fsis.Count() <= 0)
            {
                return 3;
            }
            foreach (FileSystemInfo fsi in fsis)
            {
                if (fsi is FileInfo)
                {
                    FileInfo fi = (FileInfo)fsi;
                    string[] fileNamearr = fsi.Name.Split('.');
                    if (fileNamearr.LastOrDefault() != "doc" && fileNamearr.LastOrDefault() != "docx")
                    {
                        result = 1;
                        break;
                    }
                }
            }
            return result;
            //MessageBox.Show("有不支持的文件类型！请检查PrepareFile目录里面的文件");
        }

        /// <summary>
        /// 把文件加载到ListBox
        /// </summary>
        private void LoadWord()
        {
            listBoxFile.Items.Clear();
            DirectoryInfo mydir = new DirectoryInfo(path);
            foreach (FileSystemInfo fsi in mydir.GetFileSystemInfos())
            {
                if (fsi is FileInfo)
                {
                    FileInfo fi = (FileInfo)fsi;
                    listBoxFile.Items.Add(fi.Name);
                }
            }
        }

        /// <summary>
        /// 开始 启动一个线程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnLoadFile_Click(new object(), new EventArgs());
            if (!string.IsNullOrEmpty(this.cmbPrinter.Text))
            {
                isStart = true;
                t = new Thread(new ThreadStart(PrintMain));
                t.Start();
                string message = DateTime.Now.ToString("MM-dd HH:mm:ss") + "打印开始……";
                listBoxLog.Items.Add(message);
                LogManage.WriteLog(LogManage.LogFile.Trace, message);
                this.btnStart.Enabled = false;
                this.btnBreak.Enabled = true;
                this.btnPause.Enabled = true;
            }
            else
            {
                MessageBox.Show("请选择一个打印机！");
                this.cmbPrinter.DroppedDown = true;
            }
        }

        /// <summary>
        /// 停止方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBreak_Click(object sender, EventArgs e)
        {
            isStart = false;
            this.btnStart.Enabled = true;
            this.btnBreak.Enabled = false;
            string message = DateTime.Now.ToString("MM-dd HH:mm:ss") + "程序终止……";
            listBoxLog.Items.Add(message);
            LogManage.WriteLog(LogManage.LogFile.Trace, message);
        }

        private void WritePauseLog()
        {
            while (pause)
            {
                DateTime now = DateTime.Now;
                now = now.AddMinutes(Convert.ToInt32(txtRestTime));
                string message = now.ToString("MM-dd HH:mm:ss") + "程序暂停中：" + now.Subtract(DateTime.Now).ToString();
                this.listBoxLog.Items.Add(message);
                LogManage.WriteLog(LogManage.LogFile.Trace, message);
            }
        }

        /// <summary>
        /// 打印主线程方法
        /// </summary>
        private void PrintMain()
        {
            try
            {
                DirectoryInfo mydir = new DirectoryInfo(path);
                FileSystemInfo[] fsis = mydir.GetFileSystemInfos();
                int i = 0;
                foreach (FileSystemInfo fsi in fsis)
                {
                    if (i != 0 && i % Convert.ToInt32(txtCount.Text) == 0)
                    {
                        listBoxLog.Items.Add(DateTime.Now.ToString("MM-dd HH:mm:ss") + "已经打印" + i + "正在休息");
                        Thread.Sleep(Convert.ToInt32(txtRestTime.Text) * 1000);
                    }
                    i++;
                    if (!isStart)
                        break;
                    if (pause)
                    {
                        pause = false;
                    }
                    if (fsi is FileInfo)
                    {
                        FileInfo fi = (FileInfo)fsi;
                        PrintHelper printHelper = new PrintHelper();
                        //printHelper.Printword(fi.FullName, txtPrintName.Text);
                        printHelper.PrintMethodOther(fi.FullName, cmbPrinter.Text);
                        string message = DateTime.Now.ToString("MM-dd HH:mm:ss") + "正在打印" + fi.Name + "……";
                        listBoxLog.Items.Add(message);
                        LogManage.WriteLog(LogManage.LogFile.Trace, message);
                    }
                    listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1;

                }
                listBoxLog.Items.Add(DateTime.Now.ToString("MM-dd HH:mm:ss") + "打印结束。");
                LogManage.WriteLog(LogManage.LogFile.Trace, DateTime.Now.ToString("MM-dd HH:mm:ss") + "打印结束。");
                this.btnStart.Enabled = true;
                this.btnBreak.Enabled = false;
                listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogManage.WriteLog(LogManage.LogFile.Error, ex.Message);
            }

        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            WritePauseLog();
            pause = true;
            this.btnBreak.Enabled = false;
            this.btnStart.Enabled = true;
            this.btnPause.Enabled = false;
        }


        /// <summary>
        /// 获取所有打印机
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllPrinter()
        {
            ManagementObjectCollection queryCollection;
            string _classname = "SELECT * FROM Win32_Printer";
            Dictionary<string, ManagementObject> dict = new Dictionary<string, ManagementObject>();
            ManagementObjectSearcher query = new ManagementObjectSearcher(_classname);
            queryCollection = query.Get();
            List<string> result = new List<string>();
            foreach (ManagementObject mo in queryCollection)
            {
                string oldName = mo["Name"].ToString();
                result.Add(oldName);
            }
            return result;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.listBoxLog.Items.Clear();
        }
    }
}


//string x = System.IO.Path.GetDirectoryName(fi.FullName);

//                    Console.WriteLine(x);
//                    string s = System.IO.Path.GetExtension(fi.FullName);
//                    string y = System.IO.Path.GetFileNameWithoutExtension(fi.FullName);
//                    Console.WriteLine(y);
//                    if (s == ".jpg")
//                    {
//                        System.IO.File.Copy(fi.FullName, x + @"\oo" + fi.Name); //在原文件名前加上OO
//                        System.IO.File.Delete(fi.FullName);
//                    }
