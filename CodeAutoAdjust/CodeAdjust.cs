﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CodeAutoAdjust.Multithread;

namespace CodeAutoAdjust
{
    public class CodeAdjust : IBackgroundExecute
    {

        public string ErrorMessage { get; private set; }
        public event UpdateStepDelegate OnUpdateStep;
        public event PerformStepDelegate OnPerformStep;

        private readonly List<string> _filePathList = new List<string>();
        private List<Tuple<string,string>> _logList=new List<Tuple<string, string>>(); 
        private string _fileFolder = null;
        public bool CodeAdjsut(string fileFolder)
        {
            _fileFolder = fileFolder;
            ProgressRun progressRun = new ProgressRun();
            if (!progressRun.Run(this, 2))
            {
                System.Windows.Forms.MessageBox.Show(ErrorMessage);
                return false;
            }
            using (StreamWriter sw = new StreamWriter("handle.log", false, Encoding.UTF8))
            {
                sw.Write(string.Join("", _logList.Select(m => m.Item2)));
                sw.Close();
            }
            return true;
        }

        public bool Exec()
        {
            try
            {

                OnUpdateStep(this, new UpdateStepEventArg() { StepMaxCount = 1, StepInfo = "取数C#文件" });
                foreach (var dir in Directory.GetDirectories(_fileFolder, "*.*", SearchOption.AllDirectories))
                {
                    if (_fileFolder.EndsWith("Services")||dir.EndsWith("Services"))
                    {
                        foreach (var file in Directory.GetFiles(dir, "*.cs", SearchOption.AllDirectories))
                        {
                            _filePathList.Add(file);
                        }
                    }
                }
                OnPerformStep(this, PerformStepEventArg.SingleStepArg);
                OnUpdateStep(this, new UpdateStepEventArg() { StepMaxCount = _filePathList.Count, StepInfo = "添加虚方法和PipelineMethod" });


                ThreadPool<String> threadPool = new ThreadPool<String>(_filePathList);
                threadPool.OnProcessData += new ThreadPool<String>.ProcessDataDelegate(HandlerFile);
                threadPool.MaxThreadCount = 4;
                threadPool.Start(false);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                return false;
            }
            return true;
        }

        public void HandlerFile(string fileName)
        {
            StringBuilder sb=new StringBuilder();
            var hasPipelineAttribute = false;
            try
            {
                string[] fileSplit = fileName.Split('\\');
                int count = fileSplit.Length;
                string shortName = fileSplit[count - 1];
                string className = shortName.Substring(0, shortName.Length - 3);
                if (shortName.IndexOf("Service", StringComparison.Ordinal) == -1 || shortName.IndexOf("AssemblyInfo", StringComparison.Ordinal) > -1)
                {
                    return;
                }
                FileInfo file = new FileInfo(fileName);
                //只读不处理
                if (file.IsReadOnly)
                {
                    return;
                }
                Encoding encoding = EncodingHelper.GetEncoding(fileName);
                string resultStr;
                using (StreamReader sr = new StreamReader(fileName, encoding))
                {
                    sb.Append("开始处理文件" + fileName).Append(Environment.NewLine);
                    List<string> fileLineList = ReadTextToList(sr.ReadToEnd());
                    int length = fileLineList.Count;
                    for (int i = 1; i < length; i++)
                    {
                        string curLine = fileLineList[i];
                        string lastLine = fileLineList[i - 1];
                        string nexLine = "";
                        if (i < length - 1)
                        {
                            nexLine = fileLineList[i + 1];
                        }
                        if (curLine.IndexOf("PipelineAttribute") > -1)
                        {
                            hasPipelineAttribute = true;
                        }
                        if (curLine.Trim().StartsWith("namespace") && hasPipelineAttribute==false)
                        {
                            sb.Append("向行【" + curLine + "】" + shortName +
          "添加命名空间Mysoft.MAP2.ServiceFramework.Core.Pipeline.PipelineAttribute的引用").Append(Environment.NewLine);
                            curLine = "using Mysoft.MAP2.ServiceFramework.Core.Pipeline.PipelineAttribute;" + Environment.NewLine + Environment.NewLine + curLine;

                        }
                        if (curLine.Trim().StartsWith("public"))
                        {
                            if (curLine.IndexOf("class") == -1 && curLine.IndexOf(":") == -1 && nexLine.IndexOf(":") == -1 && curLine.IndexOf(className) == -1)
                            {
                                //处理虚方法
                                if (curLine.IndexOf("virtual") == -1 && curLine.IndexOf("override") == -1)
                                {
                                    int index = curLine.IndexOf("public");
                                    string publicString = curLine.Substring(0, index + 6);
                                    string otherString = curLine.Substring(index + 6, curLine.Length - index - 6);
                                    sb.Append("向行【" + curLine + "】" + shortName + "添加virtual修饰符").Append(Environment.NewLine);
                                    curLine = publicString + " virtual" + otherString;
                                }

                                //处理PipelineMethod
                                if (lastLine.IndexOf("PipelineMethod") == -1)
                                {
                                    sb.Append("向行【" + curLine + "】" + shortName + "添加[PipelineMethod]修饰符").Append(Environment.NewLine); ;
                                    curLine = "        [PipelineMethod]" + Environment.NewLine + curLine;
                                }
                            }
                        }
                        fileLineList[i] = curLine;
                    }
                    sr.Close();
                    resultStr = string.Join(Environment.NewLine, fileLineList);
                }
                using (StreamWriter sw = new StreamWriter(fileName, false, encoding))
                {
                    sw.Write(resultStr);
                    sw.Close();
                }
                _logList.Add(new Tuple<string, string>(fileName, sb.ToString()));
                OnPerformStep(this, PerformStepEventArg.SingleStepArg);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }


        private List<string> ReadTextToList(string text)
        {
            return text.Split(Environment.NewLine.ToCharArray()).ToList();
        }
    }
}
