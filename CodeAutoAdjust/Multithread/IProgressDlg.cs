﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeAutoAdjust.Multithread
{
    /// <summary>
    /// 显示进度条的对话框接口
    /// </summary>
    public interface IProgressDlg
    {
        /// <summary>
        /// 获取或设置总步骤数
        /// </summary>
        int StepCount
        {
            get;
            set;
        }

        /// <summary>
        /// 获取或设置是否允许取消
        /// </summary>
        bool AllowCancel
        {
            get;
            set;
        }

        /// <summary>
        /// 递增滚动条
        /// </summary>
        void PerformStep();

        /// <summary>
        /// 根据指定的进度数来递增滚动条
        /// </summary>
        /// <param name="stepCount">要递增的进度数</param>
        void PerformStep(int stepCount);

        /// <summary>
        /// 设置显示的信息
        /// </summary>
        /// <param name="info">要显示的信息</param>
        void NextSetp(int progressCount, string info);

        IRunObject RunObject
        {
            set;
        }

        void Show();

        void Hide();
    }
}
