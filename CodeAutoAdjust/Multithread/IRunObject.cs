﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeAutoAdjust.Multithread
{
    public interface IRunObject
    {
        void Run();
    }
}
