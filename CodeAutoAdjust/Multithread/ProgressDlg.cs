﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CodeAutoAdjust.Multithread
{
    public partial class ProgressDlg : Form, IProgressDlg
    {
        private int _stepCount;
        private Thread _timeThread;
        private ThreadState _threadState;
        private int _nowStep = 0;
        private int _progressCount;
        private IRunObject _runObject;

        public ProgressDlg()
        {
            InitializeComponent();
        }

        #region IProgressDlg 成员

        /// <summary>
        /// 获取或设置总步骤数
        /// </summary>
        int IProgressDlg.StepCount
        {
            get
            {
                return _stepCount;
            }
            set
            {
                _stepCount = value;
                _nowStep = 0;
            }
        }

        /// <summary>
        /// 获取或设置是否允许取消
        /// </summary>
        bool IProgressDlg.AllowCancel
        {
            get
            {
                return this.btnCancel.Enabled;
            }
            set
            {
                this.btnCancel.Enabled = false;
            }
        }

        void IProgressDlg.PerformStep()
        {
            Interlocked.Increment(ref _progressCount);
        }

        void IProgressDlg.PerformStep(int stepCount)
        {
            Interlocked.Add(ref _progressCount, stepCount);
        }

        void IProgressDlg.NextSetp(int progressCount, string info)
        {
            this.Invoke(new Action<int, string>(NextSetp_internal), progressCount, info);
        }

        IRunObject IProgressDlg.RunObject
        {
            set
            {
                _runObject = value;
            }
        }

        void IProgressDlg.Show()
        {
            this.ShowDialog();
        }

        void IProgressDlg.Hide()
        {
            this.Invoke(new Action(Close_internal));
        }

        #endregion

        private void Close_internal()
        {
            _threadState = ThreadState.StopRequested;
            _timeThread.Abort();
            this.Close();
        }

        private void NextSetp_internal(int progressCount, string info)
        {
            _nowStep++;
            lblInfo.Text = string.Format("({0}/{1})", _nowStep, _stepCount) + info;
            progressBar1.Maximum = progressCount;
            progressBar1.Value = 0;
            Interlocked.Exchange(ref _progressCount, 0);
        }

        private void timeThreadProcess()
        {
            while (_threadState == ThreadState.Running)
            {
                Thread.Sleep(100);
                if (_progressCount > 0)
                {
                    this.Invoke(
                        new Action(PerformStep_internal)
                    );
                }
            }
            _threadState = ThreadState.Stopped;
        }

        private void PerformStep_internal()
        {
            if (_progressCount > 0)
            {
                progressBar1.Value += Interlocked.Exchange(ref _progressCount, 0);
            }
        }

        private void ProgressDlg_Load(object sender, EventArgs e)
        {
            _timeThread = new Thread(new ThreadStart(timeThreadProcess));
            _threadState = ThreadState.Running;
            _timeThread.Start();

            _runObject.Run();
        }
    }
}
