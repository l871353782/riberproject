﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CodeAutoAdjust.Multithread
{
    /// <summary>
    /// 运行进度条，并采用多线程来执行程序
    /// </summary>
    public class ProgressRun : IRunObject
    {
        public class ExecCompletedEventArg
        {
            private bool _isSucceed;
            private bool _isException;
            private string _message;

            public ExecCompletedEventArg(bool isSucceed, string message)
            {
                _isSucceed = isSucceed;
                _isException = false;
                _message = message;
            }

            public ExecCompletedEventArg(string message)
            {
                _isSucceed = false;
                _isException = true;
                _message = message;
            }

            public bool IsSucceed
            {
                get
                {
                    return _isSucceed;
                }
            }

            public bool IsException
            {
                get
                {
                    return _isException;
                }
            }

            public string Message
            {
                get
                {
                    return _message;
                }
            }
        }

        public delegate void ExecCompletedDelegate(object sender, ExecCompletedEventArg e);

        private BackgroundWorker _backgroundWorker = new BackgroundWorker();
        private bool _isExecute = false;
        private IProgressDlg _progressDlg;
        private IBackgroundExecute _backgroupExecute;
        private int _stepCount;
        private bool _isSuccess = true;
        private string _errorMessage;

        public ProgressRun()
            : this(new ProgressDlg())
        {
        }

        public ProgressRun(IProgressDlg progressDlg)
        {
            _progressDlg = progressDlg;
            _progressDlg.RunObject = this;
            _backgroundWorker.DoWork += new DoWorkEventHandler(_backgroundWorker_DoWork);
        }

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        public bool Run(IBackgroundExecute backgroupExecute, int stepCount)
        {
            if (_isExecute)
            {
                throw new System.Exception("当前后台程序正在执行操作");
            }

            _backgroupExecute = backgroupExecute;
            _stepCount = stepCount;

            _progressDlg.Show();

            return _isSuccess;
        }

        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            _isExecute = true;
            IBackgroundExecute backgroupExecute = (IBackgroundExecute)e.Argument;
            backgroupExecute.OnUpdateStep += new UpdateStepDelegate(backgroupExecute_OnUpdateStep);
            backgroupExecute.OnPerformStep += new PerformStepDelegate(backgroupExecute_OnPerformStep);
            try
            {
                if (!backgroupExecute.Exec())
                {
                    _isSuccess = false;
                    _errorMessage = backgroupExecute.ErrorMessage;
                }
            }
            catch (System.Exception ex)
            {
                _isSuccess = false;
                _errorMessage = ex.Message;
            }

            _progressDlg.Hide();
        }

        void backgroupExecute_OnPerformStep(object sender, PerformStepEventArg e)
        {
            _progressDlg.PerformStep();
        }

        void backgroupExecute_OnUpdateStep(object sender, UpdateStepEventArg e)
        {
            _progressDlg.NextSetp(e.StepMaxCount, e.StepInfo);
        }

        #region IRunObject 成员

        void IRunObject.Run()
        {
            _backgroundWorker.RunWorkerAsync(_backgroupExecute);
            _progressDlg.StepCount = _stepCount;
        }

        #endregion
    }
}
