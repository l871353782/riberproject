﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeAutoAdjust.Multithread
{
    /// <summary>
    /// 后台执行接口
    /// </summary>
    public interface IBackgroundExecute
    {
        /// <summary>
        /// 当执行失败时，获取具体的错误信息
        /// </summary>
        string ErrorMessage
        {
            get;
        }

        /// <summary>
        /// 更新步骤事件
        /// </summary>
        event UpdateStepDelegate OnUpdateStep;

        /// <summary>
        ///更新步骤内进度条事件
        /// </summary>
        event PerformStepDelegate OnPerformStep;

        /// <summary>
        /// 执行服务
        /// </summary>
        /// <returns>执行成果返回true，否则返回false</returns>
        bool Exec();
    }

    /// <summary>
    /// 更新执行步骤事件参数
    /// </summary>
    public class UpdateStepEventArg : EventArgs
    {
        public string StepInfo;

        public int StepMaxCount;
    }

    /// <summary>
    /// 步骤递增事件参数
    /// </summary>
    public class PerformStepEventArg : EventArgs
    {
        public int StepCount = 1;

        public static PerformStepEventArg SingleStepArg = new PerformStepEventArg();
    }

    /// <summary>
    /// 更新步骤委托
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The e.</param>
    public delegate void UpdateStepDelegate(object sender, UpdateStepEventArg e);

    /// <summary>
    /// 递增步骤委托
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The e.</param>
    public delegate void PerformStepDelegate(object sender, PerformStepEventArg e);
}
