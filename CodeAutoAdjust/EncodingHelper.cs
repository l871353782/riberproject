﻿using System;
using System.IO;
using System.Text;
using NChardet;

namespace CodeAutoAdjust
{
    /// <summary>
    /// 功能：获取文件编码
    /// 逻辑：先根据文件头判断，如果没有头，采用Moliza判断流编码的Chardet算法.net版本NChardet进行判断
    /// </summary>
    public class EncodingHelper
    {
        private static readonly byte[] _unicodeHeader = new byte[] {0xFF, 0xFE};
        private static readonly byte[] _unicodeBigHeader = new byte[] {0xFE, 0xFF};
        private static readonly byte[] _utf8Header = new byte[] {0xEF, 0xBB, 0xBF};

        public static Encoding GetEncoding(string path)
        {
            Encoding encoding;
            using (StreamReader sr = new StreamReader(path, Encoding.Default, true))
            {
                encoding = GetEncoding(sr);
            }

            return encoding;
        }

        public static Encoding GetEncoding(StreamReader sr)
        {
            using (BinaryReader br = new BinaryReader(sr.BaseStream, Encoding.Default))
            {
                byte[] bytes = br.ReadBytes(3);
                if (bytes.Length > 0)
                {
                    if (bytes.Length >= 3)
                    {
                        if ((bytes[0] == _utf8Header[0] && bytes[1] == _utf8Header[1] && bytes[2] == _utf8Header[2]))
                        {
                            return Encoding.UTF8;
                        }
                        if (bytes[0] == _unicodeHeader[0] && bytes[1] == _unicodeHeader[1])
                        {
                            return Encoding.Unicode;
                        }
                        if (bytes[0] == _unicodeBigHeader[0] && bytes[1] == _unicodeBigHeader[1])
                        {
                            return Encoding.BigEndianUnicode;
                        }
                    }
                    sr.BaseStream.Seek(0, SeekOrigin.Begin);
                    string encodingName = DetectStream(sr.BaseStream);  //对于某些编码、某些文件可能不准确
                    if (encodingName.Equals("ASCII"))
                    {
                        return Encoding.Default;    //ASCII文件返回Encoding.Default，即ANSI编码(CodePage936-GB2312)
                    }
                    try
                    {
                        return Encoding.GetEncoding(encodingName);
                    }
                    catch (ArgumentException)
                    {
                        return Encoding.Default;
                    }
                }
                return Encoding.Default;
            }
        }

        public static StreamReader ReadFile(string filePath)
        {
            Encoding encoding = GetEncoding(filePath);
            return new StreamReader(filePath, encoding);
        }

        public static void TranEncoding(string filePath, Encoding encoding, string desFilePath)
        {
            Encoding fileEncoding = EncodingHelper.GetEncoding(filePath);
            if (fileEncoding == encoding)
            {
                File.Copy(filePath, desFilePath, true);
                FileInfo fileInfo = new FileInfo(desFilePath);
                fileInfo.Attributes = FileAttributes.Normal;
            }
            else
            {
                string s = null;
                using (StreamReader sr = new StreamReader(filePath, fileEncoding))
                {
                    s = sr.ReadToEnd();
                    sr.Close();
                }
                using (StreamWriter sw = new StreamWriter(desFilePath, false, encoding))
                {
                    sw.Write(s);
                    sw.Close();
                }
            }
        }

        #region NChardet

        /*
        private static string DetectUrl(string path)
        {
            Uri url = new Uri(path);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();

            return DetectStream(stream);
        }

        private static string DetectFile(string path)
        {
            string result;
            using (StreamReader sr = new StreamReader(path, Encoding.Default, true))
            {
                result = DetectStream(sr.BaseStream);
            }

            return result;
        }
         */

        private static string DetectStream(Stream stream)
        {
            int lang = 2;
            Detector det = new Detector(lang);
            MyCharsetDetectionObserver cdo = new MyCharsetDetectionObserver();
            det.Init(cdo);

            byte[] buf = new byte[1024];
            int len;
            bool done = false;
            bool isAscii = true;

            while ((len = stream.Read(buf, 0, buf.Length)) != 0)
            {
                if (isAscii) isAscii = det.isAscii(buf, len);
                if (!isAscii && !done) done = det.DoIt(buf, len, false);
            }
            stream.Close();
            stream.Dispose();

            det.DataEnd();

            if (isAscii)
            {
                return "ASCII";
            }
            if (cdo.Charset != null)
            {
                return cdo.Charset;
            }
            string[] prob = det.getProbableCharsets();
            foreach (string t in prob)
            {
                return t;
            }

            return string.Empty;
        }

        #endregion
    }

    public class MyCharsetDetectionObserver : ICharsetDetectionObserver
    {
        public string Charset { get; set; }

        public void Notify(string charset)
        {
            Charset = charset;
        }
    }
}