﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.PowerPoint;



namespace AsposeOperate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.*|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtWordPath.Text = ofd.FileName;
            }
        }

        private void btnCountPage_Click(object sender, EventArgs e)
        {
            string filepath = txtWordPath.Text;
            int pageCount = 0;
            if (filepath.EndsWith(".doc") || filepath.EndsWith(".docx"))
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(filepath);
                pageCount = doc.PageCount;
            }
            else if (filepath.EndsWith(".ppt") || filepath.EndsWith(".pptx"))
            {
                   _Application ppt = new ApplicationClass();
                   Presentation pptp = ppt.Presentations.Open(filepath, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse);
                   pageCount = pptp.Slides.Count;
            }
            else if (filepath.EndsWith(".pdf"))
            {
                Aspose.Pdf.Document pdfDocument = new Aspose.Pdf.Document(filepath);
                pageCount=pdfDocument.Pages.Count;
            }
            txtPageCount.Text=pageCount.ToString();
        }
    }
}
