﻿namespace AsposeOperate
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtWordPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCountPage = new System.Windows.Forms.Button();
            this.txtPageCount = new System.Windows.Forms.TextBox();
            this.ye = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "文件：";
            // 
            // txtWordPath
            // 
            this.txtWordPath.Location = new System.Drawing.Point(101, 66);
            this.txtWordPath.Name = "txtWordPath";
            this.txtWordPath.Size = new System.Drawing.Size(131, 21);
            this.txtWordPath.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(238, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCountPage
            // 
            this.btnCountPage.Location = new System.Drawing.Point(194, 113);
            this.btnCountPage.Name = "btnCountPage";
            this.btnCountPage.Size = new System.Drawing.Size(66, 23);
            this.btnCountPage.TabIndex = 3;
            this.btnCountPage.Text = "计算";
            this.btnCountPage.UseVisualStyleBackColor = true;
            this.btnCountPage.Click += new System.EventHandler(this.btnCountPage_Click);
            // 
            // txtPageCount
            // 
            this.txtPageCount.Location = new System.Drawing.Point(133, 113);
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Size = new System.Drawing.Size(32, 21);
            this.txtPageCount.TabIndex = 4;
            // 
            // ye
            // 
            this.ye.AutoSize = true;
            this.ye.Location = new System.Drawing.Point(171, 117);
            this.ye.Name = "ye";
            this.ye.Size = new System.Drawing.Size(17, 12);
            this.ye.TabIndex = 5;
            this.ye.Text = "页";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.ye);
            this.Controls.Add(this.txtPageCount);
            this.Controls.Add(this.btnCountPage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtWordPath);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWordPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCountPage;
        private System.Windows.Forms.TextBox txtPageCount;
        private System.Windows.Forms.Label ye;

    }
}

