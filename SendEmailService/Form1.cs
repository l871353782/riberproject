﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SendEmailService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendEmailService sendEmail = new SendEmailService(txtReceive.Text, txtSend.Text, txtMainContent.Text, txtTopic.Text, EncryptService.DecryptDES("eR+rIUmQiESDpTmVwMXgDw=="));
            if (!txtAttachments.Text.Trim().Equals(""))
            {
                sendEmail.Attachments(txtAttachments.Text);
            }
            if (sendEmail.Send())
            {
                MessageBox.Show("发送成功");
            }
            ////加密解密测试
            //txtReceive.Text = EncryptService.EncryptDES(txtSend.Text);
            //txtTopic.Text = EncryptService.DecryptDES(txtReceive.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            string fileNames = "";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach(string filename in ofd.FileNames)
                {
                    fileNames += filename + ",";
                }
                txtAttachments.Text = fileNames.Substring(0, fileNames.Length - 1);
            }
        }
    }
}
