﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Cell cell = new Cell();
            cell.value = "linq";
            Row row = new Row();
            row.cells = new List<Cell>() { cell };
            Group group = new Group();
            group.rows = new List<Row>() { row };
            Region region = new Region();
            region.groups = new List<Group>() { group };
            Region region2 = new Region();
            region2.groups = new List<Group>() { group };
            Form form = new Form();
            form.regions = new List<Region>() { region, region2 };

            IEnumerable<Face2> cells = form.regions.SelectMany(m => m.groups).SelectMany(m => m.rows).SelectMany(m => m.cells).OfType<Face1>().OfType<Face2>();
            int count = cells.ToList<Face2>().Count;
        }


        private class Form
        {
            public List<Region> regions { get; set; }
        }
        private class Region
        {
            public List<Group> groups { get; set; }
        }
        private class Group
        {
            public List<Row> rows { get; set; }
        }
        private class Row
        {
            public List<Cell> cells { get; set; }
        }
        private class Cell : Face1, Face2
        {
            public string value;

            public void fn1()
            {
                throw new NotImplementedException();
            }

            public void fn2()
            {
                throw new NotImplementedException();
            }
        }
        private interface Face1
        {
            void fn1();
        }

        private interface Face2
        {
            void fn2();
        }
        
     }
}
