﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;

namespace NPOI.Extensions
{
    /// <summary>
    /// NPOI扩展
    /// </summary>
    internal static class NPOIExtension
    {
        /// <summary>
        /// 获取或创建指定的行
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public static IRow GetOrNewRow(this ISheet sheet, int rowIndex)
        {
            IRow row = sheet.GetRow(rowIndex);
            if (row == null)
            {
                row = sheet.CreateRow(rowIndex);
            }
            return row;
        }

        public static ICell GetCell(this ISheet sheet, string address)
        {
            if (address.Length < 2)
                return null;

            int index = address.IndexOf('!');
            int rowIndex = 0, cellIndex = 0;

            if (index > 0)
            {
                sheet = sheet.Workbook.GetSheet(address.Substring(0, index));
                address = address.Substring(index + 1);
            }
            else if(index == 0)
            {
                address = address.Substring(1);
            }

            for(index = 0; index < address.Length; index++)
            {
                if(address[index] < 'A')
                    break;

                cellIndex = cellIndex * 26 + (address[index] - 'A' + 1);
            }
            for(; index < address.Length; index++)
            {
                rowIndex = rowIndex * 10 + (address[index] - '0');
            }

            return sheet.GetCell(rowIndex - 1, cellIndex - 1);
        }

        /// <summary>
        /// 获取或创建指定的列
        /// </summary>
        /// <param name="row"></param>
        /// <param name="cellIndex"></param>
        /// <returns></returns>
        public static ICell GetOrNewCell(this IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            return cell;
        }

        /// <summary>
        /// 获取或创建指定的列
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="cellIndex"></param>
        /// <returns></returns>
        public static ICell GetOrNewCell(this ISheet sheet, int rowIndex, int cellIndex)
        {
            IRow row = sheet.GetRow(rowIndex);
            if (row == null)
            {
                row = sheet.CreateRow(rowIndex);
            }
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
            }
            return cell;
        }

        /// <summary>
        /// 获取列
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="cellIndex"></param>
        /// <returns></returns>
        public static ICell GetCell(this ISheet sheet, int rowIndex, int cellIndex)
        {
            IRow row = sheet.GetRow(rowIndex);
            if (row != null)
                return row.GetCell(cellIndex);

            return null;
        }

        /// <summary>
        /// 获取字符串值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static string GetStringValue(this ICell cell)
        {
            if (cell == null)
                return null;

            return GetStringValue(cell, cell.CellType);
        }

        /// <summary>
        /// 获取指定列的合并区间对象
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="cellIndex"></param>
        /// <returns></returns>
        public static CellRangeAddress GetCellRangeAddress(this ISheet sheet, int rowIndex, int cellIndex)
        {
            CellRangeAddress address;

            for(int i = 0; i < sheet.NumMergedRegions; i++)
            {
                address = sheet.GetMergedRegion(i);
                if (address.FirstRow <= rowIndex
                    && address.LastRow >= rowIndex
                    && address.FirstColumn <= cellIndex
                    && address.LastColumn >= cellIndex)
                    return address; 
            }
            return null;
        }

        private static string GetStringValue(ICell cell, CellType cellType)
        {
            switch (cellType)
            {
                case CellType.NUMERIC:
                    if (HSSFDateUtil.IsCellDateFormatted(cell))
                        return cell.DateCellValue.ToString();
                    else
                        return cell.NumericCellValue.ToString();

                case CellType.BOOLEAN:
                    return cell.BooleanCellValue ? "1" : "0";

                case CellType.FORMULA:
                    return GetStringValue(cell, cell.CachedFormulaResultType);

                default:
                    return cell.StringCellValue;
            }
        }
    }
}
