﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Text;
using System.Data.SqlClient;

using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using Mysoft.PubProject.Validation;
using Mysoft.MAP2.Infrastructure.Data.DAL;
using Mysoft.UTFramework.Extensions;

namespace NPOI
{
    internal class LoadDataToDataTable
    {
        private const string c_deleteSql = "exec sp_executesql N'DELETE FROM {0} WHERE {1}=@pk',N'@pk {2}',@pk='";
        private static readonly Type c_GuidType = typeof(Guid);
        private static readonly Type c_StringType = typeof(string);

        private string _strExcelFile;
        private Dictionary<string, string> _paramList;

        public LoadDataToDataTable(string strExcelFile, Dictionary<string, string> paramList)
        {
            ArgumentValidation.Validation("fileName", strExcelFile
                , ArgumentValidation.Helper.StringEmpty(false));

            if (!File.Exists(strExcelFile))
            {
                throw new FileNotFoundException("当前要导入的excel文件没有找到", strExcelFile);
            }

            _strExcelFile = strExcelFile;
            _paramList = paramList;
        }

        public void ImportToDataBase()
        {
            DataSet ds = LoadToDataSet();
            DataTable dt;
            int i, j, k;
            DataRow dataRow;
            CPQuery query;
            StringBuilder sbInsertSql;
            string insertSql;

            using (ConnectionScope scope = new ConnectionScope(TransactionMode.Required))
            {
                for (i = 0; i < ds.Tables.Count; i++)
                {
                    dt = ds.Tables[i];

                    if(dt.Columns.Count == 0 || dt.Rows.Count == 0)
                        continue;

                    // 1.删除已有数据
                    BuildDeleteSql(dt).AsCPQuery().ExecuteNonQuery();

                    // 2.提交数据
                    sbInsertSql = new StringBuilder();
                    sbInsertSql.AppendFormat("INSERT INTO [{0}](", dt.TableName);
                    sbInsertSql.Append('[').Append(dt.Columns[0].ColumnName).Append(']');
                    for (j = 1; j < dt.Columns.Count; j++)
                    {
                        sbInsertSql.Append(",[").Append(dt.Columns[j].ColumnName).Append(']');
                    }
                    sbInsertSql.Append(") VALUES(");
                    insertSql = sbInsertSql.ToString();

                    for (j = 0; j < dt.Rows.Count; j++)
                    {
                        dataRow = dt.Rows[j];

                        query = insertSql.AsCPQuery();
                        query += new QueryParameter(dataRow[0]);
                        for (k = 1; k < dt.Columns.Count; k++)
                        {
                            query += ",";
                            query += new QueryParameter(dataRow[k]);
                        }
                        query += ")";
                        query.ExecuteNonQuery();
                    }
                }
                //提交事务,如果不启用事务,无需Commit();
                scope.Commit();
            }
        }

        public DataSet LoadToDataSet()
        {
            HSSFWorkbook workbook;
            using (FileStream fs = new FileStream(_strExcelFile, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);

                fs.Close();
            }

            int i;
            DataSet ds = new DataSet();
            DataTable dt;

            ISheet catalogSheet = workbook.GetSheetAt(0);
            if (catalogSheet.SheetName == "目录")
            {
                string tableName, sheetName;
                ICell catalogCell;
                ISheet dataSheet;
                for (i = 0; i <= catalogSheet.LastRowNum; i++)
                {
                    catalogCell = catalogSheet.GetCell(i, 0);
                    tableName = catalogCell.GetStringValue();
                    if (string.IsNullOrEmpty(tableName))
                        break;

                    sheetName = catalogCell.Hyperlink.Address.Split('!')[0].Substring(1);
                    dataSheet = workbook.GetSheet(sheetName);
                    if (dataSheet == null)
                        throw new Exceptions.NotFoundException(string.Format("表[{0}]对应的数据sheet[{1}]没有找到.", tableName, sheetName));

                    dt = LoadToDataTable(dataSheet, tableName);
                    if (dt != null)
                    {
                        if (dt.DataSet != null)
                            dt.DataSet.Tables.Remove(dt);
                        ds.Tables.Add(dt);
                    }
                }
            }
            else
            {
                for (i = 0; i < workbook.NumberOfSheets; i++)
                {
                    dt = LoadToDataTable(workbook.GetSheetAt(i), null);
                    if (dt != null)
                    {
                        if (dt.DataSet != null)
                            dt.DataSet.Tables.Remove(dt);
                        ds.Tables.Add(dt);
                    }
                }
            }

            return ds;
        }

        public DataTable LoadToDataTable(string sheetName)
        {
            HSSFWorkbook workbook;
            using (FileStream fs = new FileStream(_strExcelFile, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);

                fs.Close();
            }
            ISheet sheet = workbook.GetSheet(sheetName);

            return sheet == null ? null : LoadToDataTable(sheet, null);
        }

        public DataTable LoadToDataTable(string sheetName, string tableName)
        {
            HSSFWorkbook workbook;
            using (FileStream fs = new FileStream(_strExcelFile, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);

                fs.Close();
            }
            ISheet sheet = workbook.GetSheet(sheetName);

            return sheet == null ? null : LoadToDataTable(sheet, tableName);
        }

        private DataTable LoadToDataTable(ISheet sheet, string tableName)
        {
            DataTable dt = CreateEmptyDataTable(sheet, tableName);

            FillData(dt, sheet);

            return dt;
        }

        private DataTable CreateEmptyDataTable(ISheet sheet, string tableName)
        {
            DataTable dt = null;
            int i, colCount;
            ICell cell;
            IRow row = sheet.GetRow(0);

            if(row == null || row.LastCellNum < 1)
                return null;

            if (string.IsNullOrEmpty(tableName))
                tableName = sheet.SheetName;

            colCount = row.LastCellNum;

            // 1.优先利用数据库中的表结构创建
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT ");
            for (i = 0; i < colCount; i++)
            {
                cell = row.GetCell(i);
                if (cell == null)
                {
                    break;
                }

                sbSql.Append(cell.GetStringValue() + ",");
            }
            colCount = i;
            if (colCount > 0)
            {
                sbSql[sbSql.Length - 1] = ' ';
                sbSql.Append("FROM ");
                sbSql.Append(tableName);
                sbSql.Append(" WHERE 1=2");

                try
                {
                    using (ConnectionScope conn = new ConnectionScope())
                    {
                        dt = sbSql.ToString().AsCPQuery().FillDataTable();
                    }
                }
                catch (Exception)
                { }
            }

            // 2.数据库中不存在对应的表,则默认所有字段均为字符串
            if (dt == null && colCount > 0)
            {
                dt = new DataTable();
                for (i = 0; i < colCount; i++)
                {
                    cell = row.GetCell(i);
                    dt.Columns.Add(new DataColumn(cell.GetStringValue(), c_StringType));
                }
            }

            dt.TableName = tableName;

            return dt;
        }

        private void FillData(DataTable dt, ISheet sheet)
        {
            string[] datas;
            DataRow dtRow;
            int colCount = dt.Columns.Count;
            int i, j;
            for (i = 1; i <= sheet.LastRowNum; i++)
            {
                datas = GetRowData(sheet.GetRow(i), colCount);
                if (datas == null)
                {
                    break;
                }

                dtRow = dt.NewRow();
                for (j = 0; j < colCount; j++)
                {
                    try
                    {
                        dtRow[j] = DataConvert.ConvertDataType(ReplaceParam(datas[j]), dt.Columns[j].DataType);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("在对表[{0}]第[{1}]行字段[{2}]的数据进行填充时,发生数据格式转换错误"
                                                    , sheet.SheetName, i, dt.Columns[j].ColumnName)
                                        , ex);
                    }
                }
                dt.Rows.Add(dtRow);
            }
        }

        private string[] GetRowData(IRow row, int colCount)
        {
            if (row == null)
                return null;

            string[] datas = new string[colCount];
            ICell cell;
            bool existsValue = false;
            for (int i = 0; i < colCount; i++)
            {
                cell = row.GetCell(i);
                if (cell == null)
                {
                    datas[i] = string.Empty;
                }
                else
                {
                    datas[i] = cell.GetStringValue();
                    if(datas[i] != null && datas[i].Trim() != string.Empty)
                        existsValue = true;
                }
            }

            return existsValue ? datas : null;
        }

        private string BuildDeleteSql(DataTable dt)
        {
            DataRow row;
            StringBuilder sbSql = new StringBuilder();
            string dbType;
            if (dt.Columns[0].DataType == c_GuidType)
            {
                dbType = "uniqueidentifier";
            }
            else
            {
                dbType = "nvarchar(1000)";
            }
            string deleteSql = string.Format(c_deleteSql, dt.TableName, dt.Columns[0].ColumnName, dbType);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                row = dt.Rows[i];
                sbSql.AppendLine(deleteSql + row[0].ToString() + "';");
            }

            return sbSql.ToString();
        }

        private string ReplaceParam(string s)
        {
            if (_paramList != null)
            {
                string value;
                if (_paramList.TryGetValue(s, out value))
                {
                    return value;
                }
            }
            return s;
        }
    }
}
